%option noyywrap
%{
#include<stdio.h>
int v=0,c=0,l=0,sp=0;
%}

%%
[\n] {l++;}
[AEIOUaeiou][A-Za-z]* {v++; printf("Vowel = %s", yytext);}
[A-Za-z]+ {c++;}
[@#$%] {sp++;}
. {return 0;}
%%

main()
{
	yyin = fopen("sample.txt", "r");
	yylex();
	printf("Vowels = %d\n",v);
	printf("Consonants = %d\n",c);
	printf("Lines = %d\n",l);
	printf("Special Symbols = %d\n",sp);
}
