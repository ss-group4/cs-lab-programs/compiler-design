
%option noyywrap
%{
#include<stdio.h>
int vow = 0, con = 0;
%}

%%
[AEIOUaeiou] {vow++;}
[A-Za-z] {con++;}
\n {return 0;}
%%

main()
{
	printf("Enter the text :: ");
	yylex();
	printf("Vowel Count = %d\n", vow);
	printf("Consonant Count = %d",con);
}

%option noyywrap
%{
#include "y.tab.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
void yyerror(const char*);
%}

%%
"print" 	{return print;}
"close"		{return close;}
[a-zA-Z]+[0-9]*		{yylval.id = strdup(yytext); return identifier;}
[0-9]+		{yylval.num = atoi(yytext); return number;}
[ \t]		;
[-+=/*%\n]	{return yytext[0];}
.	{ECHO; yyerror("Not a legal character!!!\n");exit(0);}
%%

main()
{
yylex();}

