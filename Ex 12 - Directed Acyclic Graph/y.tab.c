#ifndef lint
static const char yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93";
#endif

#include <stdlib.h>
#include <string.h>

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20100216

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)

#define YYPREFIX "yy"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
#ifdef YYPARSE_PARAM_TYPE
#define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
#else
#define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
#endif
#else
#define YYPARSE_DECL() yyparse(void)
#endif /* YYPARSE_PARAM */

extern int YYPARSE_DECL();

#line 2 "dag.y"
	#include<stdio.h>
	#include<ctype.h>
	#include<stdlib.h>
	#include<string.h>
	struct DAG
	{
		char *sym;
		int left,right;
	}table[100];
	char subexp[20][20];
	int sub_exp_tbl_idx[20];
	int tbl_entry=0,idx,sub_exp_count=0;
	int search(char *);
	int issubexp(char *);
	int search_subexp(char *);
	void add_subexp(char *);
#line 19 "dag.y"
typedef union{char *name;} YYSTYPE;
#line 53 "y.tab.c"
#define num 257
#define id 258
#define YYERRCODE 256
static const short yylhs[] = {                           -1,
    0,    0,    1,    1,    1,    1,    1,    1,    1,
};
static const short yylen[] = {                            2,
    3,    1,    3,    3,    3,    3,    3,    1,    1,
};
static const short yydefred[] = {                         0,
    9,    0,    0,    0,    0,    0,    8,    0,    0,    0,
    0,    0,    0,    7,    0,    0,    4,    6,
};
static const short yydgoto[] = {                          4,
    5,
};
static const short yysindex[] = {                       -40,
    0,  -58,  -38,    0,  -20,  -38,    0,  -26,  -38,  -38,
  -38,  -38,  -20,    0,  -18,  -18,    0,    0,
};
static const short yyrindex[] = {                         0,
    0,    1,    0,    0,    6,    0,    0,    0,    0,    0,
    0,    0,    7,    0,    4,    9,    0,    0,
};
static const short yygindex[] = {                         0,
    2,
};
#define YYTABLESIZE 220
static const short yytable[] = {                          3,
    8,    3,    6,    3,    8,    2,    1,   13,    5,    0,
   15,   16,   17,   18,   14,   11,    9,    0,   10,    0,
   12,   11,    9,   11,   10,    0,   12,    0,   12,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    8,    8,    3,    8,    3,    8,    3,    5,
    0,    5,    0,    5,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    1,    2,    1,    7,
};
static const short yycheck[] = {                         40,
    0,   40,   61,    0,    3,    0,    0,    6,    0,   -1,
    9,   10,   11,   12,   41,   42,   43,   -1,   45,   -1,
   47,   42,   43,   42,   45,   -1,   47,   -1,   47,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   42,   43,   41,   45,   43,   47,   45,   41,
   -1,   43,   -1,   45,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  257,  258,  257,  258,
};
#define YYFINAL 4
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 258
#if YYDEBUG
static const char *yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,"'('","')'","'*'","'+'",0,"'-'",0,"'/'",0,0,0,0,0,0,0,0,0,0,0,0,0,
"'='",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
"num","id",
};
static const char *yyrule[] = {
"$accept : start",
"start : id '=' e",
"start : e",
"e : e '+' e",
"e : e '*' e",
"e : e '-' e",
"e : e '/' e",
"e : '(' e ')'",
"e : id",
"e : num",

};
#endif
#if YYDEBUG
#include <stdio.h>
#endif

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 500
#define YYMAXDEPTH  500
#endif
#endif

#define YYINITSTACKSIZE 500

int      yydebug;
int      yynerrs;

typedef struct {
    unsigned stacksize;
    short    *s_base;
    short    *s_mark;
    short    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;

#define YYPURE 0

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 54 "dag.y"
int yyerror()
{
	printf("\nInvalid expr!!");
	return 0;
}
main()
{
	int i;	
	printf("\nEnter an expr:");
	yyparse();
	printf("\nDAG for the given expr is:");
	printf("\n------------------------------");
	printf("\nIndex\tNode\tLeft\tRight");
	printf("\n-------- ----------------------\n");
	for(i=0;i<tbl_entry;i++)
	{
		if(isalnum(table[i].sym[0]))		
			printf("%d\t%s\t%c\t%c\n",i,table[i].sym,table[i].left,table[i].right);
		else
			printf("%d\t%s\t%d\t%d\n",i,table[i].sym,table[i].left,table[i].right);
	}
}
int search(char *sym)
{
	int i;
	for(i=0;i<tbl_entry;i++)
		if(strcmp(table[i].sym,sym)==0)
			return i;
	return -1;
}
void addsym(char *sym, int l, int r)
{
	table[tbl_entry].sym=strdup(sym);
	table[tbl_entry].left = l;
	table[tbl_entry].right = r;
	tbl_entry++;
}
void addop(char *sym, char *l, char *r, char *exp)
{
	int l_index,r_index;

	if(!issubexp(l) && !issubexp(r))
	{	
		if(search_subexp(exp)==-1)
		{
			addsym(sym,search(l),search(r));
			add_subexp(exp);
		}
	}
	else
	{
		if(issubexp(l))
			l_index = search_subexp(l);
		else
			l_index = search(l);
		if(issubexp(r))
			r_index = search_subexp(r);
		else
			r_index = search(r);
		addsym(sym,l_index,r_index);
		add_subexp(exp);
	}
}
int search_subexp(char *exp)
{
	int i;
	for(i=0;i<sub_exp_count;i++)
	{
		if(strcmp(subexp[i],exp)==0)
			return sub_exp_tbl_idx[i];
	}
	return -1;
}
int issubexp(char *exp)
{
	int i,len=strlen(exp);
	for(i=0;i<len;i++)
		if(exp[i]=='+' || exp[i] == '-' || exp[i] == '*' || exp[i] == '/')
			return 1;
	return 0;
}
void add_subexp(char *exp)
{
	strcpy(subexp[sub_exp_count],exp);
	sub_exp_tbl_idx[sub_exp_count] = tbl_entry - 1;
	sub_exp_count++;
}

#line 290 "y.tab.c"
/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    short *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return -1;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = data->s_mark - data->s_base;
    newss = (data->s_base != 0)
          ? (short *)realloc(data->s_base, newsize * sizeof(*newss))
          : (short *)malloc(newsize * sizeof(*newss));
    if (newss == 0)
        return -1;

    data->s_base  = newss;
    data->s_mark = newss + i;

    newvs = (data->l_base != 0)
          ? (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs))
          : (YYSTYPE *)malloc(newsize * sizeof(*newvs));
    if (newvs == 0)
        return -1;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack)) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    yyerror("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 1:
#line 26 "dag.y"
	{ 
					idx = search(yystack.l_mark[-2].name);
					if(idx == -1)
						addsym(yystack.l_mark[-2].name,45,45);
					if(issubexp(yystack.l_mark[0].name))
						addsym("=",search(yystack.l_mark[-2].name),search_subexp(yystack.l_mark[0].name));
					else
						addsym("=",search(yystack.l_mark[-2].name),search(yystack.l_mark[0].name));
				}
break;
case 2:
#line 35 "dag.y"
	{}
break;
case 3:
#line 37 "dag.y"
	{yyval.name = strdup(yystack.l_mark[-2].name); strcat(yyval.name,"+"); strcat(yyval.name,yystack.l_mark[0].name); addop("+",yystack.l_mark[-2].name,yystack.l_mark[0].name,yyval.name);}
break;
case 4:
#line 38 "dag.y"
	{yyval.name = strdup(yystack.l_mark[-2].name); strcat(yyval.name,"*"); strcat(yyval.name,yystack.l_mark[0].name); addop("*",yystack.l_mark[-2].name,yystack.l_mark[0].name,yyval.name);}
break;
case 5:
#line 39 "dag.y"
	{yyval.name = strdup(yystack.l_mark[-2].name); strcat(yyval.name,"-"); strcat(yyval.name,yystack.l_mark[0].name); addop("-",yystack.l_mark[-2].name,yystack.l_mark[0].name,yyval.name);}
break;
case 6:
#line 40 "dag.y"
	{yyval.name = strdup(yystack.l_mark[-2].name); strcat(yyval.name,"/"); strcat(yyval.name,yystack.l_mark[0].name); addop("/",yystack.l_mark[-2].name,yystack.l_mark[0].name,yyval.name);}
break;
case 7:
#line 41 "dag.y"
	{yyval.name = strdup(yystack.l_mark[-1].name);}
break;
case 8:
#line 42 "dag.y"
	{
					idx = search(yystack.l_mark[0].name);
					if(idx == -1)
						addsym(yystack.l_mark[0].name,45,45);
				}
break;
case 9:
#line 47 "dag.y"
	{
					idx = search(yystack.l_mark[0].name);
					if(idx == -1)
						addsym(yystack.l_mark[0].name,45,45);
				}
break;
#line 544 "y.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (short) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    yyerror("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
