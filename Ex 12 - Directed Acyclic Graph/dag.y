%{
	#include<stdio.h>
	#include<ctype.h>
	#include<stdlib.h>
	#include<string.h>
	struct DAG
	{
		char *sym;
		int left,right;
	}table[100];
	char subexp[20][20];
	int sub_exp_tbl_idx[20];
	int tbl_entry=0,idx,sub_exp_count=0;
	int search(char *);
	int issubexp(char *);
	int search_subexp(char *);
	void add_subexp(char *);
%}
%union{char *name;}
%token <name>  num id
%type <name> e start
%left '+' '-'
%left '*' '/'
%right '='
%%
start	:	id '=' e	{ 
					idx = search($1);
					if(idx == -1)
						addsym($1,45,45);
					if(issubexp($3))
						addsym("=",search($1),search_subexp($3));
					else
						addsym("=",search($1),search($3));
				}
	|	e		{}
;
e	:	e '+' e		{$$ = strdup($1); strcat($$,"+"); strcat($$,$3); addop("+",$1,$3,$$);}
	|	e '*' e		{$$ = strdup($1); strcat($$,"*"); strcat($$,$3); addop("*",$1,$3,$$);}
	|	e '-' e		{$$ = strdup($1); strcat($$,"-"); strcat($$,$3); addop("-",$1,$3,$$);}
	|	e '/' e		{$$ = strdup($1); strcat($$,"/"); strcat($$,$3); addop("/",$1,$3,$$);}
	|	'(' e ')'	{$$ = strdup($2);}
	|	id		{
					idx = search($1);
					if(idx == -1)
						addsym($1,45,45);
				}
	|	num		{
					idx = search($1);
					if(idx == -1)
						addsym($1,45,45);
				}
;
%%
int yyerror()
{
	printf("\nInvalid expr!!");
	return 0;
}
main()
{
	int i;	
	printf("\nEnter an expr:");
	yyparse();
	printf("\nDAG for the given expr is:");
	printf("\n------------------------------");
	printf("\nIndex\tNode\tLeft\tRight");
	printf("\n-------- ----------------------\n");
	for(i=0;i<tbl_entry;i++)
	{
		if(isalnum(table[i].sym[0]))		
			printf("%d\t%s\t%c\t%c\n",i,table[i].sym,table[i].left,table[i].right);
		else
			printf("%d\t%s\t%d\t%d\n",i,table[i].sym,table[i].left,table[i].right);
	}
}
int search(char *sym)
{
	int i;
	for(i=0;i<tbl_entry;i++)
		if(strcmp(table[i].sym,sym)==0)
			return i;
	return -1;
}
void addsym(char *sym, int l, int r)
{
	table[tbl_entry].sym=strdup(sym);
	table[tbl_entry].left = l;
	table[tbl_entry].right = r;
	tbl_entry++;
}
void addop(char *sym, char *l, char *r, char *exp)
{
	int l_index,r_index;

	if(!issubexp(l) && !issubexp(r))
	{	
		if(search_subexp(exp)==-1)
		{
			addsym(sym,search(l),search(r));
			add_subexp(exp);
		}
	}
	else
	{
		if(issubexp(l))
			l_index = search_subexp(l);
		else
			l_index = search(l);
		if(issubexp(r))
			r_index = search_subexp(r);
		else
			r_index = search(r);
		addsym(sym,l_index,r_index);
		add_subexp(exp);
	}
}
int search_subexp(char *exp)
{
	int i;
	for(i=0;i<sub_exp_count;i++)
	{
		if(strcmp(subexp[i],exp)==0)
			return sub_exp_tbl_idx[i];
	}
	return -1;
}
int issubexp(char *exp)
{
	int i,len=strlen(exp);
	for(i=0;i<len;i++)
		if(exp[i]=='+' || exp[i] == '-' || exp[i] == '*' || exp[i] == '/')
			return 1;
	return 0;
}
void add_subexp(char *exp)
{
	strcpy(subexp[sub_exp_count],exp);
	sub_exp_tbl_idx[sub_exp_count] = tbl_entry - 1;
	sub_exp_count++;
}

