%option noyywrap
%{	
  #include "y.tab.h"
  extern YYSTYPE  yylval;
%}
%%
[_a-zA-Z][a-zA-Z0-9]* {yylval.name=strdup(yytext);return id;}
[0-9]+ 			{yylval.name=strdup(yytext);return num;}
[ \t]*			{}
[\n] 			{return 0;}
. 			{return yytext[0];} 
%%
