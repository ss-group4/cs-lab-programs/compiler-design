#include<stdio.h>
#include<string.h>

main()
{
	char data[4][10] = {"int", "float", "char", "double"};
	char *p, *type;
	char line[15][100];
	
	FILE *fp;
	fp = fopen("test.c", "r");

	int i = 0, j, depth = 0;	
	while(!feof(fp))
	{
		fgets(line[i], 100, fp);
		i++;
	}

	printf("Identifier\tType\tAddress\t\tDepth\n\n");
	for(j = 0; j < i; j++)
	{
		if(strncmp(line[j],"{",1) == 0)
			depth++;
		if(strncmp(line[j],"}",1) == 0)
			depth--;

		p = strtok(line[j], " ");
		if((strcmp(p, "int") == 0) || (strcmp(p, "float") == 0) || (strcmp(p, "double") == 0) || (strcmp(p, "float") == 0)) 
		{
			type = p;
			while((p = strtok(NULL, ",;\n")) != NULL)
			{
				printf("%s\t\t%s\t%p\t\t%d\n",type, p, p, depth);
			}
		}
	}
} 	
