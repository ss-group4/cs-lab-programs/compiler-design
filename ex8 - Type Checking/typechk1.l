%option noyywrap
%{
	#include "y.tab.h"
	extern char *yylval;
	extern char *toktype;
	int lineno=1;
%}
%%
\'.\'					{yylval=strdup(yytext);return CH;}
"int"|"char"|"float"|"double"|"long" 	{toktype=strdup(yytext);yylval=strdup(yytext);return TYPE;}
[_a-zA-Z][_a-zA-Z0-9]*			{yylval=strdup(yytext);return ID;}
[0-9]+[\.0-9]* 				{yylval=strdup(yytext);return NUM;}
[ \t]+ 					;
\n					lineno++;
. 					return yytext[0];
%%
