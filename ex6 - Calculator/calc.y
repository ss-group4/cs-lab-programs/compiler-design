%{
        int yylex();
        void yyerror (char *);
        #include <stdio.h>
        #include <string.h>
        #define MAX 100
        #define YYERROR_VERBOSE
        char sym[MAX][MAX];
        int symval[MAX], n=0,err=0;
        int SearchSymtab(char *);
        void UpdateSymtab(char *, int);
%}

%union {int num; char *id;}
%start line
%token print
%token bye
%token <num> number
%token <id> identifier
%type <num> line exp term
%type <id> assignment
%left '+' '-'
%left '*' '/' '%'
%nonassoc UMINUS

%%
line    : assignment '\n'       {;}
        | bye '\n'              { exit(0); }
        | print exp '\n'        { if(err)err=0;  else printf("%d\n", $2); }
	| line assignment '\n'  {;}
        | line print exp '\n'   { if(err)err=0;  else printf("%d\n", $3); }
        | line bye '\n' { exit(0); }
	;

assignment : identifier '=' exp { if(err) ; else UpdateSymtab($1,$3); }
           ;
exp     : term                  { if(err) ; else $$ = $1; }
        | exp '+' exp           { $$ = $1 + $3; }
        | exp '-' exp           { $$ = $1 - $3; }
        | exp '*' exp           { $$ = $1 * $3; }
        | exp '/' exp           { if ($3!=0)
                                        $$ = $1 / $3;
                                  else
                                  {
                                        yyerror("Divide by zero!!\n");
                                        exit(0);
                                  }
                                }
        | exp '%' exp           { if ($3!=0)
                                        $$ = $1 % $3;
                                  else
                                  {
                                        yyerror("Divide by zero!!\n");
                                        exit(0);
                                  }
                                }
        | '-' exp %prec UMINUS  { $$ = -$2; }
        | '(' exp ')'           { $$ = $2; }
        ;

term    : number                { $$ = $1; }
        | identifier            { int idx;
                                  if((idx = SearchSymtab($1))!=-1)
                                  {     $$=idx;}
                                  else
                                  {printf("Undefined Symbol %s\n",$1);err=1;}
                                }
        ;
%%
int main(void)
{
        printf("Calculator application\n------------------\n");
        printf("Sample Expressions: a = 10, c = a + 10\n");
        printf("To print a variable: print a \n");
        printf("type 'exit' to Quit\n-------------------------\n");
        yyparse();
        return 0;
}

void yyerror(char *s)
{
        fprintf(stderr,"%s",s);
}

int checksym(char *symbol)
{
        int i;
        for (i=0;i<=n;i++)
                if (strcmp(symbol,sym[i])==0)
                        return i;
        return -1;
}

int SearchSymtab(char *symbol)
{
        int index = checksym(symbol);
        if(index==-1)
                return index;
        else
                return symval[index];
}

void UpdateSymtab(char *symbol, int val)
{
        int index = checksym(symbol);
        if (index != -1)
                symval[index] = val;
        else
        {
                strcpy(sym[n],symbol);
                symval[n] = val;
                n++;
        }
}
