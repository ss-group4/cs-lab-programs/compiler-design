%option noyywrap
%{
        #include "y.tab.h"
        #include <string.h>
        #include <stdlib.h>
        void yyerror (const char *);
%}

%%

"print"         { return print; }
"exit"          { return bye; }
[a-zA-Z]+[0-9]* { yylval.id = strdup(yytext); return identifier; }
[0-9]+          { yylval.num = atoi(yytext); return number; }
[ \t]           ;
[-+=/*%\n]      { return yytext[0]; }
.               { ECHO; yyerror("Not a Legal Character!!\n"); exit(0); }

%%


