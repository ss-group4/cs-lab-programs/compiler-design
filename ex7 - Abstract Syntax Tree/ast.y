%{
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int flag=1,i=0;
struct astnode
{
	char *data;
	struct astnode *left,*right;
}*root;
%}
%union{ struct astnode *node;
	char *name;
}
%token <name>  num id
%type <node> e start s
%left '+' '-'
%left '*' '/'
%right '='
%%
start:s '=' e  {$$=addnode($1,$3,"=");root=$$;}
| e  {$$=$1;root=$$;}
;
e:s  		{$$=$1;}
 |e '+' e	{$$=addnode($1,$3,"+");}
 |e '*' e	{$$=addnode($1,$3,"*");}
 |e '-' e	{$$=addnode($1,$3,"-");}
 |e '/' e	{$$=addnode($1,$3,"/");}
 |'('e')'	{$$=$2;}
s:id		{$$=makenode($1);}
 |num		{$$=makenode($1);}
;
%%
int yyerror()
{
int flag=0;
printf("invalid expr!!");
return 0;
}
main()
{
	void printasttree(struct astnode *);
	printf("enter expr:");
	yyparse();
	printf("\nPreorder traversal of the AST tree for the given expression is:");
	printasttree(root);
	printf("\n");
}
struct astnode *addnode(struct astnode *l,struct astnode *r,char *sym)
{
  	struct astnode *temp;
	temp = (struct astnode *)malloc(sizeof(struct astnode));
	temp->data=strdup(sym);
	temp->left=l;
        temp->right=r;
        return temp;
}

struct astnode *makenode(char *data)
{
	struct astnode *temp;
	temp = (struct astnode *)malloc(sizeof(struct astnode));
	temp->data=strdup(data);
	temp->left=NULL;
	temp->right=NULL;
	return temp;
}
void printasttree(struct astnode *s)
{
	if(s!=NULL)
	{
		printf("%s",s->data);
	}
	if(s->left!=NULL)
		printasttree(s->left);
	if(s->right!=NULL)
		printasttree(s->right);
}
