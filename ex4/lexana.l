%option noyywrap
%{
#include<stdio.h>
int l=1;
%}
%%
\n {l++;}
[#][a-zA-Z<>.]+ {printf("preprocessor:%s line:%d\n",yytext,l);}
[A-Za-z]+[()]+ {printf("function:%s line:%d\n",yytext,l);}
"int"|"float"|"double"|"char"|"if"|"else" {printf("keyword:%s line:%d\n",yytext,l);}
[a-zA-Z_]+[A-Za-z0-9]* {printf("identifier:%s line:%d\n",yytext,l);}
. {}
%%
main()
{
yyin=fopen("file.txt","r");
yylex();
}
